import { parse } from 'date-fns';

import { History } from '../../models/history';
import { UserAdapter } from './user.adapter';

export class HistoryAdapter {
    static parseAPIResponse(data: any): History {
        return new History({
            version: data.version,
            url: data.url,
            committedAt: parse(data.committed_at),
            user: UserAdapter.parseAPIResponse(data.user)
        });
    }
}
