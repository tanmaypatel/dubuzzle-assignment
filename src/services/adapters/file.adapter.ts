import { File } from '../../models/file';

export class FileAdapter {
    static parseAPIResponse(data: any): File {
        return new File({
            filename: data.filename,
            type: data.type,
            language: data.language,
            url: data.raw_url,
            size: data.size
        });
    }
}
