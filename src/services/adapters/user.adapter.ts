import { User } from '../../models/user';

export class UserAdapter {
    static parseAPIResponse(data: any): User {
        return new User({
            id: data.id,
            login: data.login,
            avatar: data.avatar_url,
            url: data.html_url
        });
    }
}
