import { extend, map, orderBy } from 'lodash';
import { parse } from 'date-fns';

import { GistDetails } from '../../models/gist-details';
import { GistAdapter } from './gist.adapter';
import { ForkAdapter } from './fork.adapter';
import { HistoryAdapter } from './history.adapter';
import { Fork } from '../../models/fork';

export class GistDetailsAdapter {
    static parseAPIResponse(data: any): GistDetails {

        const forks: Fork[] = map(data.forks, (datum: any) => {
            return ForkAdapter.parseAPIResponse(datum);
        });

        return new GistDetails(extend(GistAdapter.prepareGistData(data), {
            forks: orderBy(forks, (datum: Fork) => {
                return datum.createdAt.valueOf();
            }, 'desc'),
            history: map(data.history, (datum: any) => {
                return HistoryAdapter.parseAPIResponse(datum);
            }),
        }));
    }
}
