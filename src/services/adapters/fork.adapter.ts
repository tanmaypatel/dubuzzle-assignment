import { parse } from 'date-fns';

import { Fork } from '../../models/fork';
import { UserAdapter } from './user.adapter';

export class ForkAdapter {
    static parseAPIResponse(data: any): Fork {
        return new Fork({
            id: data.id,
            url: data.url,
            createdAt: parse(data.created_at),
            updatedAt: parse(data.updated_at),
            user: UserAdapter.parseAPIResponse(data.user)
        });
    }
}
