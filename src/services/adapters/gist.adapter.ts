import { map } from 'lodash';
import { parse } from 'date-fns';

import { Gist, IGist } from '../../models/gist';
import { FileAdapter } from './file.adapter';
import { UserAdapter } from './user.adapter';

export class GistAdapter {

    static prepareGistData(data: any): IGist {
        return {
            id: data.id,
            description: data.description,
            url: data.url,
            htmlUrl: data.html_url,
            forksUrl: data.forks_url,
            commitsUrl: data.commits_url,
            files: map(data.files, (datum: any) => {
                return FileAdapter.parseAPIResponse(datum);
            }),
            owner: UserAdapter.parseAPIResponse(data.owner),
            createdAt: parse(data.created_at, null),
            updatedAt: parse(data.updated_at, null)
        };
    }

    static parseAPIResponse(data: any): Gist {
        return new Gist(GistAdapter.prepareGistData(data));
    }
}
