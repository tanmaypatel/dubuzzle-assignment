import * as Bluebird from 'bluebird';
import { map } from 'lodash';
import 'whatwg-fetch';

import { Config } from '../config/config';
import { Gist } from '../models/gist';
import { GistAdapter } from './adapters/gist.adapter';
import { GistDetailsAdapter } from './adapters/gist-details.adapter';
import { GistDetails } from '../models/gist-details';

export class GistsService {
    static retrieveGistsForUser(userId: string): Bluebird<Gist[]> {
        return Bluebird.try(() => {
            return new Request(
                `${Config.GISTS_API_BASE_URL}/users/${userId}/gists`,
                {
                    headers: {
                        'Authorization': `Basic ${btoa(`${Config.GISTS_API_AUTHORIZATION_USERNAME}:${Config.GISTS_API_AUTHORIZATION_TOKEN}`)}`
                    }
                }
            );
        })
            .then((request: Request) => {
                return fetch(request);
            })
            .then((response: Response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.statusText);
                }
            })
            .then((responseBody: any) => {
                const gists: Gist[] = map(
                    responseBody,
                    (datum: any) => {
                        return GistAdapter.parseAPIResponse(datum);
                    }
                );

                return gists;
            });
    }

    static retrieveForksForGist(gist: Gist): Bluebird<any> {
        return Bluebird.try(() => {
            return new Request(
                gist.forksUrl,
                {
                    headers: {
                        'Authorization': `Basic ${btoa(`${Config.GISTS_API_AUTHORIZATION_USERNAME}:${Config.GISTS_API_AUTHORIZATION_TOKEN}`)}`
                    }
                }
            );
        })
            .then((request: Request) => {
                return fetch(request);
            })
            .then((response: Response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.statusText);
                }
            })
            .then((responseBody: any) => {
                const gists: Gist[] = map(
                    responseBody,
                    (datum: any) => {
                        return GistAdapter.parseAPIResponse(datum);
                    }
                );

                return gists;
            });
    }

    static retrieveGistDetails(gistId: string): Bluebird<GistDetails> {
        return Bluebird.try(() => {
            return new Request(
                `${Config.GISTS_API_BASE_URL}/gists/${gistId}`,
                {
                    headers: {
                        'Authorization': `Basic ${btoa(`${Config.GISTS_API_AUTHORIZATION_USERNAME}:${Config.GISTS_API_AUTHORIZATION_TOKEN}`)}`
                    }
                }
            );
        })
            .then((request: Request) => {
                return fetch(request);
            })
            .then((response: Response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error(response.statusText);
                }
            })
            .then((responseBody: any) => {
                const gist: GistDetails = GistDetailsAdapter.parseAPIResponse(responseBody);

                return gist;
            });   
    }
}
