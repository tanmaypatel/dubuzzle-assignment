export enum DeferredStatus {
    NOT_STARTED = Number.NEGATIVE_INFINITY,
    IN_PROGRESS = -1,
    FAILED = 0,
    SUCCEEDED = 1
}
