import { User } from './user';

export interface IHistory {
    version: string;
    user: User;
    url: string;
    committedAt: Date;
}

export class History {
    readonly version: string = '';
    readonly user: User = null;
    readonly url: string = '';
    readonly committedAt: Date = null;

    constructor(data: IHistory) {
        this.version = data.version;
        this.user = data.user;
        this.url = data.url;
        this.committedAt = data.committedAt;
    }
}
