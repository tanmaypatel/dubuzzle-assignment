export interface IFile {
    filename: string;
    type: string;
    language: string;
    url: string;
    size: number;
}

export class File {
    readonly filename: string = '';
    readonly type: string = '';
    readonly language: string = '';
    readonly url: string = '';
    readonly size: number = 0;

    constructor(data: IFile) {
        this.filename = data.filename;
        this.type = data.type;
        this.language = data.language;
        this.url = data.url;
        this.size = data.size;
    }
}
