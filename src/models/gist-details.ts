import { uniq, map, orderBy, take } from 'lodash';

import { File } from './file';
import { User } from './user';
import { IGist, Gist } from './gist';
import { Fork } from './fork';
import { History } from './history';

export interface IGistDetails extends IGist {
    forks: Fork[];
    history: History[];
}

export class GistDetails extends Gist {
    readonly forks: Fork[] = [];
    readonly history: History[] = [];

    get latest3Forks(): Fork[] {
        return take(this.forks, 3);
    }

    constructor(data: IGistDetails) {
        super(data);

        this.forks = data.forks;
        this.history = data.history;
    }
}
