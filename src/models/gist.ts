import { uniq, map, filter } from 'lodash';

import { File } from './file';
import { User } from './user';

export interface IGist {
    id: string;
    description: string;
    url: string;
    htmlUrl: string;
    forksUrl: string;
    commitsUrl: string;
    files: File[];
    owner: User;
    createdAt: Date;
    updatedAt: Date;
}

export class Gist {

    static NO_LANGUAGE_LABEL: string = 'None';

    readonly id: string = '';
    readonly description: string = '';
    readonly url: string = '';
    readonly htmlUrl: string = '';
    readonly forksUrl: string = '';
    readonly commitsUrl: string = '';
    readonly files: File[] = [];
    readonly owner: User = null;
    readonly createdAt: Date = null;
    readonly updatedAt: Date = null;

    get distinctLanguagesOfFiles(): string[] {
        let fileTypes: string[] = map(this.files, (datum: File) => {
            return datum.language ? datum.language : Gist.NO_LANGUAGE_LABEL;
        });

        return uniq(fileTypes);
    }

    getNumberOfFilesForLanguage(language: string): number {

        if(language === Gist.NO_LANGUAGE_LABEL) {
            language = null;
        }

        const matchedFiles: File[] = filter(this.files, (datum: File) => {
            return datum.language === language;
        });

        return matchedFiles ? matchedFiles.length : 0;
    }

    constructor(data: IGist) {
        this.id = data.id;
        this.description = data.description;
        this.url = data.url;
        this.htmlUrl = data.htmlUrl;
        this.forksUrl = data.forksUrl;
        this.commitsUrl = data.commitsUrl;
        this.files = data.files;
        this.owner = data.owner;
        this.createdAt = data.createdAt;
        this.updatedAt = data.updatedAt;
    }
}
