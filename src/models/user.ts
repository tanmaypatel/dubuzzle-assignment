export interface IUser {
    id: number;
    login: string;
    avatar: string;
    url: string;
}

export class User {
    id: number = 0;
    login: string = '';
    avatar: string = '';
    url: string = '';

    constructor(data: IUser) {
        this.id = data.id;
        this.login = data.login;
        this.avatar = data.avatar;
        this.url = data.url;
    }
}
