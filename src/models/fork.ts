import { User } from './user';

export interface IFork {
    id: string;
    user: User;
    url: string;
    createdAt: Date;
    updatedAt: Date;
}

export class Fork {
    readonly id: string = '';
    readonly user: User = null;
    readonly url: string = '';
    readonly createdAt: Date = null;
    readonly updatedAt: Date = null;

    constructor(data: IFork) {
        this.id = data.id;
        this.user = data.user;
        this.url = data.url;
        this.createdAt = data.createdAt;
        this.updatedAt = data.updatedAt;
    }
}
