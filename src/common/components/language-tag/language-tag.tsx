import * as React from 'react';
import { Component } from 'react';
import { Label, Icon, SemanticCOLORS } from 'semantic-ui-react';

interface IProps {
    language: string;
    numberOfFiles: number;
}

interface IState {
    color: SemanticCOLORS;
}

class LanguageTag extends Component<IProps, IState> {
    state: IState = {
        color: 'grey'
    };

    static getColorForLanguage(language: string): SemanticCOLORS {
        if (!language) {
            language = '';
        }

        switch (language.toLowerCase()) {
            case 'javascript':
            case 'json':
            case 'ini':
                return 'yellow';
            case 'html':
            case 'xml':
                return 'orange';
            case 'sql':
                return 'green';
            case 'sass':
            case 'scss':
                return 'pink';
            case 'python':
            case 'java':
            case 'less':
                return 'blue';
            default:
                return 'grey';
        }
    }

    static getDerivedStateFromProps(nextProps: any, prevState: IState) {
        return {
            color: LanguageTag.getColorForLanguage(nextProps.language)
        };
    }

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <Label color={this.state.color}>
                <Icon name="file code outline" /> {this.props.language}
                <Label.Detail>{this.props.numberOfFiles}</Label.Detail>
            </Label>
        );
    }
}

export default LanguageTag;
