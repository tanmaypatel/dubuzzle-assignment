import * as React from 'react';
import { Component, Fragment } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Container, Item, Label, Icon, Button, Responsive } from 'semantic-ui-react';
import { distanceInWords } from 'date-fns';
import * as PropTypes from 'prop-types';
import { map } from 'lodash';

import { Gist } from '../../../models/gist';
import LanguageTag from '../language-tag/language-tag';

import './gist-summary.scss';

interface IProps {
    gist: Gist;
}

interface IContext {
    router: any;
}

class GistSummary extends Component<IProps, any> {

    context: IContext;

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <Item className="gist-summary">
                <Responsive as={Item.Image} minWidth={768} size='tiny' src={this.props.gist.owner.avatar} />

                <Item.Content>
                    <Item.Header as={Link} to={`/gists/${this.props.gist.id}`}>
                        {this.props.gist.files[0].filename}
                    </Item.Header>
                    <Item.Description>
                        <p>{this.props.gist.description}</p>
                        <div>{map(this.props.gist.distinctLanguagesOfFiles, (datum: string) => {
                            return (<LanguageTag 
                                key={`gist_${this.props.gist.id}_${datum}`} 
                                language={datum} 
                                numberOfFiles={this.props.gist.getNumberOfFilesForLanguage(datum)} 
                            />);
                        })}</div>
                    </Item.Description>
                    <Item.Extra>
                        Updated {distanceInWords(this.props.gist.updatedAt, new Date())} ago
                    </Item.Extra>
                    <Item.Extra>
                        <Button basic primary compact as={Link} to={`/gists/${this.props.gist.id}`}>
                            Show Details
                            <Icon name="chevron right" />
                        </Button>
                    </Item.Extra>
                </Item.Content>
            </Item>
        );
    }
}

export default GistSummary;
