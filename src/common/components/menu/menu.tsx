import * as React from 'react';
import { Component, Fragment, RefObject, MouseEvent } from 'react';
import { Link, withRouter } from 'react-router-dom';
import {
    Container,
    Menu,
    Item,
    Input,
    Button,
    Form,
    ButtonProps
} from 'semantic-ui-react';
import * as PropTypes from 'prop-types';

import './menu.scss';

interface IProps {
    history: any;
}

interface IState {
    searchTerm: string;
    shouldShowSearchBar: boolean;
}

interface IContext {
    router: any;
}

class MenuBar extends Component<IProps, IState> {
    context: IContext;

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    state: IState = {
        searchTerm: '',
        shouldShowSearchBar: false
    };

    constructor(props: any) {
        super(props);

        this._onFormSearchSubmit = this._onFormSearchSubmit.bind(this);
        this._onInputSearchChange = this._onInputSearchChange.bind(this);
    }

    componentWillMount() {

        this._updateStateFromRoute(this.props.history.location.pathname);

        this.props.history.listen((location: any, action: string) => {
            this._updateStateFromRoute(location.pathname);
        });
    }

    private _updateStateFromRoute(path: string) {
        let matches: string[] = path.match(
            /^\/users\/(.+?)\/gists$/
        );

        if (matches) {
            this.setState({
                searchTerm: matches[1]
            });
        }

        if (path.match(/^\/$/)) {
            this.setState({
                shouldShowSearchBar: false
            });
        } else {
            this.setState({
                shouldShowSearchBar: true
            });
        }
    }

    private _onFormSearchSubmit(
        event: React.SyntheticEvent<any>,
        data: any
    ) {
        this.context.router.history.push(
            `/users/${this.state.searchTerm}/gists`
        );
    }

    private _onInputSearchChange(event: React.SyntheticEvent<any>, data: any) {
        this.setState({
            searchTerm: data.value
        });
    }

    render() {
        return (
            <Menu fixed="top" inverted stackable>
                <Container>
                    <Menu.Item header as={Link} to="/">Gists for dubizzle</Menu.Item>
                    {this.state.shouldShowSearchBar ? 
                        <Menu.Item position="right">
                            <Form fluid onSubmit={this._onFormSearchSubmit}>
                                <Form.Field>
                                    <Input
                                        value={this.state.searchTerm}
                                        onChange={this._onInputSearchChange}
                                        placeholder="Search Gists For..."
                                        action={
                                            <Button
                                                primary
                                                type="submit"
                                                icon="search"
                                            />
                                        }
                                    />
                                </Form.Field>
                            </Form>
                        </Menu.Item> : 
                        null
                    }
                </Container>
            </Menu>
        );
    }
}

export default withRouter(MenuBar);
