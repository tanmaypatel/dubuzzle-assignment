import * as React from 'react';
import * as ReactDOM from 'react-dom';
import 'babel-polyfill';
import 'core-js/es6/symbol';

import '../node_modules/semantic-ui-css/semantic.min.css';

import { App } from './app/App';

import './index.scss';

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('/service-worker.js')
            .then(registration => {
                console.log('SW registered: ', registration);
            })
            .catch(registrationError => {
                console.log('SW registration failed: ', registrationError);
            });
    });
}

ReactDOM.render(<App />, document.getElementById('root'));
