import * as React from 'react';
import { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Container, Item, Label, Icon, Card, List, Image, Button, Segment, Loader, Responsive } from 'semantic-ui-react';
import { distanceInWords } from 'date-fns';
import { map } from 'lodash';

import { GistsService } from '../../services/gists.service';
import { File } from '../../models/file';
import { GistDetails } from '../../models/gist-details';
import { DeferredStatus } from '../../models/deferred-status';
import GistSummary from '../../common/components/gist-summary/gist-summary';
import LanguageTag from '../../common/components/language-tag/language-tag';

import './gist-details.page.scss';
import { Fork } from '../../models/fork';
import { Gist } from '../../models/gist';

interface IState {
    gistId: string,
    retrivalStatus: DeferredStatus,
    gist: GistDetails,
    retrivalError: string
};

export class GistDetailsComponent extends Component<{
    gist: GistDetails
}, {}> {
    render() {
        return (
            <div className="row">
                <div className="col-12 col-md-4">
                    <Item.Group>
                        <Item>
                            <Responsive as={Item.Image} minWidth={992} size='tiny' src={this.props.gist.owner.avatar} />
                            <Item.Content>
                                <Item.Header>
                                    {this.props.gist.description || this.props.gist.files[0].filename}
                                </Item.Header>
                                <Item.Meta>Created <strong>{distanceInWords(this.props.gist.createdAt, new Date())} ago</strong></Item.Meta>
                                <Item.Meta>By <Link to={`/users/${this.props.gist.owner.login}/gists`}>{this.props.gist.owner.login}</Link></Item.Meta>
                                <Item.Extra>Revised <strong>{this.props.gist.history.length}</strong> times</Item.Extra>
                                <Item.Extra>Last Updated {distanceInWords(this.props.gist.updatedAt, new Date())} ago</Item.Extra>
                                <Item.Extra>
                                    <Button basic primary compact as={Link} to={`/users/${this.props.gist.owner.login}/gists`}>
                                        Show Gists by {this.props.gist.owner.login}
                                    </Button>
                                </Item.Extra>
                            </Item.Content>
                        </Item>
                    </Item.Group>
                </div>
                <div className="col-12 col-md-8">
                    <Card fluid>
                        <Card.Content header={`${this.props.gist.files.length} Files`} />
                        <Card.Content>
                            <List divided >
                            {map(this.props.gist.files, (datum: File) => {
                                return (
                                    <List.Item key={`gist_${this.props.gist.id}_file_${datum.filename}`}>
                                        <Label basic color={LanguageTag.getColorForLanguage(datum.language)} horizontal>{datum.language || Gist.NO_LANGUAGE_LABEL}</Label>
                                        <a href={datum.url} target="_blank">{datum.filename} <Icon name="external" /></a>
                                    </List.Item>
                                );
                            })}
                            </List>
                        </Card.Content>
                        <Card.Content extra>
                            {map(this.props.gist.distinctLanguagesOfFiles, (datum: string) => {
                                return (<LanguageTag 
                                    key={`gist_${this.props.gist.id}_language_${datum}`}
                                    language={datum}
                                    numberOfFiles={this.props.gist.getNumberOfFilesForLanguage(datum)}
                                />);
                            })}
                        </Card.Content>
                    </Card>

                    <Card fluid>
                        <Card.Content header={`${this.props.gist.forks.length} Forks`} />
                        <Card.Content>

                            {!this.props.gist.forks.length ? 
                                <Segment secondary>There are no forks!</Segment> :
                                null
                            }

                            {this.props.gist.forks.length && this.props.gist.forks.length > this.props.gist.latest3Forks.length ? 
                                <Segment secondary>
                                    Showing latest <strong>{this.props.gist.latest3Forks.length}</strong> forks from total <strong>{this.props.gist.forks.length}</strong> forks
                                </Segment> :
                                null
                            }

                            <List divided relaxed='very'>
                            {map(this.props.gist.latest3Forks, (datum: Fork) => {
                                return (<List.Item key={`gist_${this.props.gist.id}_fork_${datum.id}`}>
                                    <Image avatar src={datum.user.avatar} />
                                    <List.Content>
                                        <List.Header as={Link} to={`/users/${datum.user.login}/gists`}>{datum.user.login}</List.Header>
                                        <List.Description>Last Updated <strong>{distanceInWords(datum.updatedAt, new Date())}</strong> ago</List.Description>
                                    </List.Content>
                                    <List.Content floated='right'>
                                        <Button primary basic compact as={Link} to={`/gists/${datum.id}`}>
                                            Show
                                            <Icon name="chevron right" />
                                        </Button>
                                    </List.Content>
                                </List.Item>);
                            })}
                            </List>
                        </Card.Content>
                    </Card>
                </div>
            </div>
        );
    }
}

export class GistDetailsPage extends Component {

    state: IState = {
        gistId: '',
        gist: null,
        retrivalStatus: DeferredStatus.NOT_STARTED,
        retrivalError: ''
    };
    
    static getDerivedStateFromProps(nextProps: any, prevState: IState) {
        return {
            gistId: nextProps.match.params.gistId
        };
    }

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this._retrieveGistDetails();
    }    

    componentDidUpdate(prevProps: any, prevState: IState, snapshot: any) {

        if(this.state.gistId !== prevState.gistId) {
            this._retrieveGistDetails();
        }
    }

    private _retrieveGistDetails() {
        this.setState({
            retrivalStatus: DeferredStatus.IN_PROGRESS
        });
        GistsService.retrieveGistDetails(this.state.gistId)
            .then((gist: GistDetails) => {
                this.setState({
                    retrivalStatus: DeferredStatus.SUCCEEDED,
                    gist: gist
                });
            })
            .catch((error: Error) => {
                this.setState({
                    retrivalStatus: DeferredStatus.FAILED,
                    gist: null,
                    retrivalError: 'Not able to retrieve Gist for given Id'
                });
            });
    }

    render() {
        return (
            <main className="container page-gist-details">

                {this.state.retrivalStatus === DeferredStatus.IN_PROGRESS ?
                    <Loader inverted>Loading Gist</Loader> :
                    null
                }

                {this.state.retrivalStatus === DeferredStatus.SUCCEEDED ? 
                    <GistDetailsComponent gist={this.state.gist} /> :
                    null
                }

                {this.state.retrivalStatus === DeferredStatus.FAILED ?
                    <Segment color='red'>{this.state.retrivalError}</Segment> :
                    null
                }

            </main>
        );
    }
}
