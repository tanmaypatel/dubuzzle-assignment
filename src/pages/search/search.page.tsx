import * as React from 'react';
import { Component, Fragment } from 'react';
import { Container, Item, Header, Loader, Segment } from 'semantic-ui-react';
import { map } from 'lodash';

import { GistsService } from '../../services/gists.service';
import { Gist } from '../../models/gist';
import { DeferredStatus } from '../../models/deferred-status';
import GistSummary from '../../common/components/gist-summary/gist-summary';

import './search.page.scss';

interface IState {
    username: string,
    searchStatus: DeferredStatus
    gists: Gist[],
    searchError: string
};

export class SearchPage extends Component {

    state: IState = {
        username: '',
        gists: null,
        searchStatus: DeferredStatus.NOT_STARTED,
        searchError: ''
    };
    
    static getDerivedStateFromProps(nextProps: any, prevState: IState) {
        return {
            username: nextProps.match.params.username
        };
    }

    constructor(props: any) {
        super(props);
    }

    componentDidMount() {
        this._retrieveGists();
    }    

    componentDidUpdate(prevProps: any, prevState: IState, snapshot: any) {

        if(this.state.username !== prevState.username) {
            this._retrieveGists();
        }
    }

    private _retrieveGists() {
        this.setState({
            searchStatus: DeferredStatus.IN_PROGRESS,
            searchError: ''
        });
        GistsService.retrieveGistsForUser(this.state.username)
            .then((gists: Gist[]) => {
                this.setState({
                    searchStatus: DeferredStatus.SUCCEEDED,
                    gists: gists
                });
            })
            .catch((error: Error) => {
                this.setState({
                    searchStatus: DeferredStatus.FAILED,
                    gists: null,
                    searchError: 'Not able to find gists for given user'
                });
            });
    }

    render() {
        return (
            <main className="container page-search-results">
                <Header as="h1">
                    Gists by {this.state.username} 
                    &nbsp;
                    <Loader size="tiny" inline active={this.state.searchStatus === DeferredStatus.IN_PROGRESS} />
                    {this.state.searchStatus === DeferredStatus.SUCCEEDED ?
                        <Header.Subheader>
                            <strong>{this.state.gists.length}</strong> Gists Found
                        </Header.Subheader> :
                        null
                    }
                </Header>

                {this.state.searchStatus === DeferredStatus.FAILED ?
                    <Segment color='red'>{this.state.searchError}</Segment> :
                    null
                }

                {this.state.searchStatus === DeferredStatus.SUCCEEDED && !this.state.gists.length ? 
                    <Segment secondary>There are no Gists created by this user!</Segment> :
                    null
                }

                <Item.Group divided>
                    {map(this.state.gists, (datum: Gist) => {
                        return <GistSummary key={`gist_${datum.id}`} gist={datum} />;
                    })}
                </Item.Group>
            </main>
        );
    }
}
