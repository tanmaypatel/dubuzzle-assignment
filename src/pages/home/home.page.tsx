import * as React from 'react';
import { Component, Fragment, RefObject, MouseEvent } from 'react';
import { Link } from 'react-router-dom';
import { Card, Icon, Input, Button, ButtonProps, Form } from 'semantic-ui-react';
import * as PropTypes from 'prop-types';

import './home.page.scss';

interface IState {
    searchTerm: string;
}

interface IContext {
    router: any;
}

export class HomePage extends Component<{}, IState> {

    context: IContext;

    static contextTypes = {
        router: PropTypes.object.isRequired
    };

    state: IState = {
        searchTerm: ''
    };

    constructor(props: any) {
        super(props);

        this._onFormSearchSubmit = this._onFormSearchSubmit.bind(this);
        this._onInputSearchChange = this._onInputSearchChange.bind(this);
    }

    private _onInputSearchChange(event: React.SyntheticEvent<any>, data: any) {
        this.setState({
            searchTerm: data.value
        });
    }

    private _onFormSearchSubmit(
        event: React.SyntheticEvent<any>,
        data: any
    ) {
        this.context.router.history.push(
            `/users/${this.state.searchTerm}/gists`
        );
    }

    render() {
        return (
            <main className="page-home">
                <div className="container">
                    <div className="row justify-content-center align-items-center">
                        <div className="col col-md-6">
                            <Card.Group>
                                <Card fluid>
                                    <Card.Content header='Search for a Github User' />
                                    <Card.Content>
                                        <Form onSubmit={this._onFormSearchSubmit}>
                                            <Form.Field>
                                                <Input
                                                    fluid
                                                    value={this.state.searchTerm}
                                                    onChange={this._onInputSearchChange}
                                                    placeholder="Search Gists For..."
                                                    action={
                                                        <Button
                                                            primary
                                                            type="submit"
                                                            icon="search"
                                                            content="Go"
                                                        />
                                                    }
                                                />
                                            </Form.Field>
                                        </Form>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Icon name='user' /> The input must be a Github User name.
                                    </Card.Content>
                                </Card>
                            </Card.Group>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}
