import * as React from 'react';
import { Component, Fragment, RefObject, MouseEvent } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import { Container } from 'semantic-ui-react';

import { HomePage } from '../pages/home/home.page';
import { SearchPage } from '../pages/search/search.page';
import MenuBar from '../common/components/menu/menu';

import './app.scss';
import { GistDetailsPage } from '../pages/gist-details/gist-details.page';

export class App extends Component {
    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <Fragment>
                <Router hashType="hashbang">
                    <Fragment>
                        <MenuBar />

                        <div className="container page-wrapper">
                            <Switch>
                                <Route 
                                    exact 
                                    path="/" 
                                    component={HomePage} 
                                />
                                <Route
                                    exact
                                    path="/gists/:gistId"
                                    component={GistDetailsPage}
                                />
                                <Route
                                    exact
                                    path="/users/:username/gists"
                                    component={SearchPage}
                                />
                            </Switch>
                        </div>
                    </Fragment>
                </Router>
            </Fragment>
        );
    }
}
