# Gists for dubizzle - Assignment for dubizzle Senior Front-end Engineer Position

## Technologies
The app is primarily created using React JS with TypeScript as its base technology, with following other technologies.
* React Router 4 for Application Routing
* Semantic UI as UI Component Framework
* Bootstrap 4 Grid System, for Grid Layout (as partial SASS imports)
* npm, as package manager
* Webpack 4, as Build tool and Bundling tool, which is also a build tool being used by dubizzle Team
* `Add to Home` with Web Manifest!
* Experimental PWA support with Workbox!

## How to Run 
1. Run `npm install` in project folder
2. Run `npm run serve` to run project locally, which will be available on `http://localhost:8080`
3. Run `npm run build` to build project in production mode. Build artifacts will be available in `/build` directory